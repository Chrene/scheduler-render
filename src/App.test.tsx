import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { SchedulerProvider } from './contexts/SchedulerContext';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SchedulerProvider><App /></SchedulerProvider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
