import React from "react";
import logo from "./assets/images/anna.png";
import "./App.css";
import { TimeslotPicker } from "./components/TimeslotPicker";
import { useScheduler } from "./contexts/SchedulerContext";

const App: React.FC = () => {
  const { submitted } = useScheduler();
  return (
    <div className="App">
      <header className="App-header">
        {!submitted && (
          <>
            <img src={logo} className="App-logo" alt="logo" />
            <h1>Hello Candidate</h1>
            <p>
              We would like to schedule an interview with you.
              <br />
              Please choose below one from the available time slots to schedule
              your interview.
            </p>
          </>
        )}
      </header>
      <main className="App-main">
        <TimeslotPicker />
      </main>
    </div>
  );
};

export default App;
