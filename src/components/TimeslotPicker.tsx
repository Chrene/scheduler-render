import "./TimeslotPicker.css";
import "react-awesome-button/dist/styles.css";
import {
  useScheduler,
  TimeslotWithId,
  TimeslotDayGroup
} from "../contexts/SchedulerContext";

import React, { useState } from "react";
import { format } from "date-fns";
import { Timeslot } from "../services/scheduler/scheduler.service";
const { AwesomeButtonProgress } = require("react-awesome-button");
export interface TimeslotPickerProps {}

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const TimeslotPicker: React.FC<TimeslotPickerProps> = props => {
  const {
    availableTimeslots,
    submitTimeslot,
    isSubmitting,
    submitted,
    bookedTimeslot
  } = useScheduler();
  const [selectedTimeslot, setSelectedTimeslot] = useState<
    TimeslotWithId | undefined
  >();
  const [selectedDayId, setSelectedDayId] = useState();
  const [submitButtonHidden, setSubmitButtonHidden] = useState(false);
  const [filteredTimeslots, setFilteredTimeslots] = useState<
    TimeslotWithId[] | undefined
  >();

  const handleDayClick = (payload: Partial<TimeslotDayGroup>) => {
    setSelectedDayId(payload.id);
    setFilteredTimeslots(payload.timeslots);
    setSelectedTimeslot(undefined);
  };

  const submit = async (timeslot?: Timeslot) => {
    if (timeslot) {
      await submitTimeslot({
        startTime: timeslot.startTime,
        endTime: timeslot.endTime
      });
      setSelectedTimeslot(undefined);
      setSelectedDayId(null);
    }
  };

  const renderTimeslots = () => {
    return (
      <>
        {availableTimeslots &&
          availableTimeslots.map(({ year, months, id }) => {
            return (
              <div key={id}>
                {months.map(({ days, month, id }) => {
                  return (
                    <div key={id}>
                      <h3>
                        {monthNames[month].toUpperCase()} / {year}
                      </h3>
                      <div className="timeslot-picker-days">
                        {days.map(({ day, weekDay, timeslots, id }) => {
                          return (
                            <div
                              key={id}
                              className={`timeslot-picker-days-day ${
                                selectedDayId === id ? "active" : ""
                              }`.trim()}
                              onClick={() => handleDayClick({ timeslots, id })}
                            >
                              <div className="day-dow">{day}</div>
                              <div className="day-name">
                                {dayNames[weekDay]}
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  );
                })}
              </div>
            );
          })}

        {selectedDayId && filteredTimeslots && (
          <div className="timeslot-picker">
            <h3>Choose a time</h3>
            <div className="timeslot-picker-days">
              {filteredTimeslots.map(timeslot => {
                return (
                  <div
                    onClick={() => {
                      setSelectedTimeslot(timeslot);
                    }}
                    key={timeslot.id}
                    className={`timeslot-picker-days-day ${
                      selectedTimeslot && selectedTimeslot.id === timeslot.id
                        ? "active"
                        : ""
                    }`.trim()}
                  >
                    <div className="day-dow">
                      {format(timeslot.startTime, "HH:mm")} -{" "}
                      {format(timeslot.endTime, "HH:mm")}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        )}
        <br />
      </>
    );
  };

  const renderDone = () => {
    return (
      <div className={"time-picked fade-in"}>
        <h3>Thank you.</h3>
        {bookedTimeslot && (
          <div>
            <p>Your booking has been confirmed at the following date</p>
            <br />
            <h3>{format(bookedTimeslot.startTime, "MMM YYY")}</h3>
            <h4>
              {format(bookedTimeslot.startTime, "HH:mm")} -{" "}
              {format(bookedTimeslot.endTime, "HH:mm")}
            </h4>
          </div>
        )}
      </div>
    );
  };

  return (
    <>
      {submitted ? renderDone() : renderTimeslots()}
      <div className={submitButtonHidden ? "hidden" : ""}>
        <AwesomeButtonProgress
          type={"primary"}
          loadingLabel="Wait.."
          resultLabel="Success"
          releaseDelay={isSubmitting ? 1000 : 2000}
          size={"large"}
          action={async (_: unknown, next: (...args: unknown[]) => unknown) => {
            const selected = selectedTimeslot
              ? { ...selectedTimeslot }
              : undefined;
            await submit(selected);
            setTimeout(async () => {
              if (selected) {
                setTimeout(async () => {
                  setSubmitButtonHidden(true);
                }, 1500);
                next();
              } else {
                next(false, "Select a time slot");
              }
            }, 500);
          }}
        >
          Submit
        </AwesomeButtonProgress>
      </div>
    </>
  );
};
