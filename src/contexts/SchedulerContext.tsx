import React, { useState, useEffect, useCallback } from "react";
import {
  Timeslot,
  getAvailableTimeslots,
  Booking
} from "../services/scheduler/scheduler.service";
import { default as AllBookings } from "../model/bookings";
import preferences from "../model/preferences";
import { daySettingsFromPreferences } from "../utils/factories";
import { addDays, getYear, getMonth, getDate, getDay } from "date-fns";
import { generate as generateId } from "shortid";
import { findOrCreate } from "../utils/find-or-create";

export type TimeslotWithId = Timeslot & { id: string };

export type TimeslotDayGroup = {
  day: number;
  weekDay: number;
  timeslots: TimeslotWithId[];
  id: string;
};

export type TimeslotMonthGroup = {
  month: number;
  days: TimeslotDayGroup[];
  id: string;
};

export type TimeslotYearGroup = {
  year: number;
  months: TimeslotMonthGroup[];
  id: string;
};

export type TimeslotGroup = TimeslotYearGroup[];

export interface SchedulerContextValue {
  availableTimeslots: TimeslotGroup;
  loadTimeslots: () => void;
  error?: Error;
  submitTimeslot: (timeslot: Timeslot) => Promise<void>;
  isSubmitting: boolean;
  submitted: boolean;
  bookedTimeslot?: Timeslot;
}

const SchedulerContext = React.createContext<SchedulerContextValue | undefined>(
  undefined
);

export interface SchedulerProviderProps {}

export const SchedulerProvider: React.FC<SchedulerProviderProps> = ({
  ...props
}) => {
  const [availableTimeslots, setTimeslots] = useState<TimeslotGroup>([]);
  const [bookings, setBookings] = useState<Booking[]>([]);
  const [isSubmitting, setSubmitting] = useState<boolean>(false);
  const [submitted, setIsSubmitted] = useState<boolean>(false);
  const [bookedTimeslot, setBookedTimeslot] = useState<Timeslot | undefined>();

  const loadTimeslots = useCallback(() => {
    const timeslots = getAvailableTimeslots({
      bookings: bookings,
      days: daySettingsFromPreferences(preferences),
      fromDate: new Date(),
      toDate: addDays(new Date(), preferences.fromTodayBoundary),
      interval: preferences.interval
    });
    setTimeslots(groupTimeslots(timeslots));
  }, [bookings]);

  const loadBookings = () => {
    setBookings([...AllBookings]);
  };

  useEffect(() => {
    loadBookings();
  }, []);

  useEffect(() => {
    loadTimeslots();
  }, [loadTimeslots]);

  const submitTimeslot = async (timeslot: Timeslot) => {
    setSubmitting(true);
    await new Promise(resolve =>
      setTimeout(resolve, Math.random() * (2000 - 500) + 500)
    );
    setSubmitting(false);
    setIsSubmitted(true);
    setBookings([...bookings, timeslot]);
    setBookedTimeslot(timeslot);
  };

  const groupTimeslots = (timeslots: Timeslot[]): TimeslotGroup => {
    return timeslots
      .sort((a, b) => a.startTime.getTime() - b.startTime.getTime())
      .reduce<TimeslotGroup>((group, timeslot) => {
        const currentYear = getYear(timeslot.startTime);
        const currentMonth = getMonth(timeslot.startTime);
        const currentDay = getDate(timeslot.startTime);

        findOrCreate(
          findOrCreate(
            findOrCreate(group, x => x.year === currentYear, {
              year: currentYear,
              months: [],
              id: generateId()
            }).months,
            x => x.month === currentMonth,
            {
              month: currentMonth,
              days: [],
              id: generateId()
            }
          ).days,
          x => x.day === currentDay,
          {
            day: currentDay,
            timeslots: [],
            weekDay: getDay(timeslot.startTime),
            id: generateId()
          }
        ).timeslots.push({
          startTime: timeslot.startTime,
          endTime: timeslot.endTime,
          id: generateId()
        });

        return group;
      }, []);
  };

  const value = {
    availableTimeslots,
    loadTimeslots,
    submitTimeslot,
    isSubmitting,
    submitted,
    bookedTimeslot
  };
  return <SchedulerContext.Provider value={value} {...props} />;
};

export function useScheduler() {
  const context = React.useContext(SchedulerContext);

  if (!context) {
    throw new Error("useScheduler can only be used within a SchedulerProvider");
  }

  return context;
}
