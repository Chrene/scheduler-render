import { getAvailableTimeslots } from "./scheduler.service";
import {
  startOfDay,
  addDays,
  addHours,
} from "date-fns";
import { daySettingsFromPreferences } from "../../utils/factories";

// Return the minutes difference between two dates
const minutesBetween = (a: Date, b: Date) =>
  Math.abs((b.getTime() - a.getTime()) / (1000 * 60));

  // Return the minutes passed since start of day
const minutesSinceStartOfDay = (a: Date) => minutesBetween(startOfDay(a), a)

const testPreferences = {
  interval: 60,
  fromTodayBoundary: 7,
  monday: {
    startTimeInMinutes: 600, // 10:00
    endTimeInMinutes: 1200 // 20:00
  },
  tuesday: {
    startTimeInMinutes: 600, // 10:00
    endTimeInMinutes: 960 // 16:00
  },
  wednesday: {
    startTimeInMinutes: 600, // 10:00
    endTimeInMinutes: 1200 // 20:00
  },
  thursday: {
    startTimeInMinutes: 600, // 10:00
    endTimeInMinutes: 960 // 16:00
  },
  friday: {
    startTimeInMinutes: 600, // 10:00
    endTimeInMinutes: 1200 // 20:00
  }
};

const testBookings = (referenceDate: Date) => [
  {
    startTime: addHours(addDays(referenceDate, 1), 10),
    endTime: addHours(addDays(referenceDate, 1), 12)
  },
  {
    startTime: addHours(addDays(referenceDate, 1), 13),
    endTime: addHours(addDays(referenceDate, 1), 14)
  },
  {
    startTime: addHours(addDays(referenceDate, 2), 10),
    endTime: addHours(addDays(referenceDate, 2), 12)
  },
  {
    startTime: addHours(addDays(referenceDate, 3), 10),
    endTime: addHours(addDays(referenceDate, 3), 12)
  },
  {
    startTime: addHours(addDays(referenceDate, 4), 10),
    endTime: addHours(addDays(referenceDate, 4), 12)
  }
];

it("respects preference boundaries", () => {
  const referenceDate = new Date("2020/01/01"); // Wednesday
  const timeslots = getAvailableTimeslots({
    interval: testPreferences.interval,
    fromDate: startOfDay(referenceDate),
    toDate: startOfDay(
      addDays(referenceDate, testPreferences.fromTodayBoundary)
    ),
    days: daySettingsFromPreferences(testPreferences),
    bookings: []
  });

  expect(timeslots.length).toEqual(52);
});

it("respects preference weekday hours boundaries", () => {
  const referenceDate = new Date("2020/01/01"); // Wednesday
  const wednesdayTimeslots = getAvailableTimeslots({
    interval: testPreferences.interval,
    fromDate: startOfDay(referenceDate),
    toDate: startOfDay(addDays(referenceDate, 0)), // 1 week including
    days: daySettingsFromPreferences(testPreferences),
    bookings: []
  });

  expect(wednesdayTimeslots.length).toEqual(10);
  expect(
    wednesdayTimeslots.every(
      timeslot =>
        minutesSinceStartOfDay(timeslot.startTime) >=
        testPreferences.wednesday.startTimeInMinutes
    )
  ).toEqual(true);
  expect(
    wednesdayTimeslots.every(
      timeslot =>
        minutesSinceStartOfDay(timeslot.endTime) <=
        testPreferences.wednesday.endTimeInMinutes
    )
  ).toEqual(true);
});

it("returns only available timeslots", () => {
  const referenceDate = new Date("2020/01/01"); // Wednesday
  const timeslots = getAvailableTimeslots({
    interval: testPreferences.interval,
    fromDate: startOfDay(referenceDate),
    toDate: startOfDay(
      addDays(referenceDate, testPreferences.fromTodayBoundary)
    ),
    days: daySettingsFromPreferences(testPreferences),
    bookings: testBookings(referenceDate)
  });

  expect(timeslots.length).toEqual(47);
});
