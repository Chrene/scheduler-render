import {
  addDays,
  areIntervalsOverlapping,
  startOfDay,
  getDay,
  addMinutes,
  differenceInDays
} from "date-fns";

export interface DaySetting {
  dayOfWeek: number;
  startTime: number;
  endTime: number;
}

export interface Booking {
  startTime: Date;
  endTime: Date;
}

export interface GetAvailableTimeslotsParams {
  interval: number;
  fromDate?: Date;
  toDate: Date;
  days: DaySetting[];
  bookings: Booking[];
}

export type Timeslot = {
  startTime: Date;
  endTime: Date;
};

const isAvailable = (timeslot: Timeslot, bookings: Booking[]): boolean => {
  return bookings.every(
    booking =>
      !areIntervalsOverlapping(
        {
          start: timeslot.startTime,
          end: timeslot.endTime
        },
        {
          start: booking.startTime,
          end: booking.endTime
        }
      )
  );
};

export const getAvailableTimeslots = (
  params: GetAvailableTimeslotsParams
): Timeslot[] => {
  const from = startOfDay(params.fromDate || new Date());
  let timeslots: Timeslot[] = [];

  for (let i = 0; i <= differenceInDays(startOfDay(params.toDate), from); i++) {
    const nextDate = addDays(from, i);
    const dayOfWeek = getDay(nextDate);
    const daySetting = params.days.find(
      element => element.dayOfWeek === dayOfWeek
    );

    if (daySetting != null) {
      for (
        let j = daySetting.startTime;
        j < daySetting.endTime;
        j += params.interval
      ) {
        const timeslot = {
          startTime: addMinutes(nextDate, j),
          endTime: addMinutes(nextDate, j + params.interval)
        };
        timeslots.push(timeslot);
      }
    }
  }

  const result = timeslots.filter(timeslot =>
    isAvailable(timeslot, params.bookings)
  );
  return result;
};
