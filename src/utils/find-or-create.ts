export function findOrCreate<T>(
  dataSource: T[],
  predicate: (element: T, index: number, obj: T[]) => T | unknown,
  defaults: T
): T {
  let element = dataSource.find(predicate);
  if (!element) {
    element = { ...defaults };
    dataSource.push(defaults);
  }
  return element;
}
