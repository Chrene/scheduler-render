import { DaySetting } from "../services/scheduler/scheduler.service";

const weekDayFromName = (str: string) => {
  switch (str.toLowerCase()) {
    case "mon":
    case "monday":
      return 1;
    case "tue":
    case "tuesday":
      return 2;
    case "wed":
    case "wednesday":
      return 3;
    case "thu":
    case "thursday":
      return 4;
    case "fri":
    case "friday":
      return 5;
    case "sat":
    case "saturday":
      return 6;
    case "sun":
    case "sunday":
      return 0;
    default:
      throw new Error(`${str} is not a valid weekday`);
  }
};

type DayPref = Object & {
  startTimeInMinutes: number;
  endTimeInMinutes: number;
};

interface Pref extends Object {
  [key: string]: DayPref | Object | undefined;
}

export const daySettingsFromPreferences = (preferences: Pref): DaySetting[] => {
  let days: DaySetting[] = [];
  Object.keys(preferences).forEach(key => {
    if (preferences.hasOwnProperty(key)) {
      if (preferences[key] != null) {
        const day = preferences[key] as DayPref;
        if (
          day &&
          day.hasOwnProperty("startTimeInMinutes") &&
          day.hasOwnProperty("endTimeInMinutes")
        ) {
          days.push({
            dayOfWeek: weekDayFromName(key),
            startTime: day.startTimeInMinutes,
            endTime: day.endTimeInMinutes
          });
        }
      }
    }
  });

  return days;
};
