# Challenge accepted!

So what we are going to do is related to scheduling ones more! this time though we are gonna dig into to one of the implementation details.

## Specs

What we are building here is the rendering engine of timeslots by availability derived from the `preferences` of the restaurant and the existing `bookings` (which mainly consists of `startTime` and `endTime` date objects in the future)

The preferences consists is an object of the `interval` meaning he timeslot size, the days the user prefers with the start and end time of for the given day declared in minutes (`startTimeInMinutes` and `endTimeInMinutes`).
There is also a property for controlling the boundary, which means the limit, in days, you should be able to schedule ahead of time from the current day.

From this you should be able to render the prefered days and timeslots in a UI where a user would be able to select them and submit them.

## Requirements

- Use the defined data structures (can be found the the `model` directory)
- Use TypeScript
- Show your React skills!
- (optional): write tests

## Get started

In the project directory, you can run:

### `npm/yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm/yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
